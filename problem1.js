const Books = require('./PromiseProblem.js');

function signIn(userId){
    let currentTime = new Date();
    return new Promise((resolve, reject) => {
        if(currentTime.getSeconds() > 30){
            resolve();
        } else{
            reject({code: 233, message: 'sign-In - failed'});
        }
    })
}

function getBooks(){
    return new Promise((resolve, reject) => {
        if((Math.random() * 1) == 1){
            resolve(Books);
        } else{
            reject({code: 123, message: 'get Books - Failed'});
        }
    })
}

function logData(activity){
    return new Promise((resolve) => {
        setTimeout(() => {
        resolve(activity);
    }, 1000);
    })
}

function userOperation(userName){
    let logPromises = [];
    signIn(userName)
    .then(() => {
        logPromises.push(logData('sig-In - successful'));
        return getBooks()
    })
    .then((books) => {
        logPromises.push(logData('get books - successful'));
        console.log(books)
    })
    .catch((err) => logPromises.push(logData(err['message'])))
    .then(() => Promise.all(logPromises))
    .then((log) => console.log(log, userName));  
}

module.exports = { userOperation };